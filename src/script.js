

const searchForm = document.querySelector('form');
const searchResult = document.querySelector(".search-result");
const container = document.querySelector(".container");
let searchQuery = "";
const APP_ID = "042c66f0";
const APP_key = "61c99c988c020dba4c1f13a98cbefe5d";

searchForm.addEventListener('submit', (e) => {
    e.preventDefault();
    searchQuery = e.target.querySelector('input').value;
    fetchAPI();
    

})



async function fetchAPI() {
    const baseURL = `https://api.edamam.com/search?q=${searchQuery}&app_id=${APP_ID}&app_key=${APP_key}`
    const response = await fetch(baseURL)
    const data = await response.json();
    generateHTML(data.hits);
    

    
}

function generateHTML(results) {
    container.classList.remove("initial")
    let generatedHTML = "";
    console.log(results);
   
    
    results.map(result => {
        generatedHTML +=
            ` 
                <div class="item">
                <img src=${result.recipe.image}>
                <div class="flex-container">
                    <h1 class="title">${result.recipe.label}</h1>
                    <a class="view-btn" href=${result.recipe.url} target="_blank">View</a>
                </div>
                <p class="item-data">Calories: ${result.recipe.calories.toFixed(2)}</p>
                <p class="item-data">Diet label: ${result.recipe.dietLabels.length>0?result.recipe.dietLabels:'No data found'}</p>
                <p class="item-data">Health label: ${result.recipe.healthLabels}</p>
            </div>
            `
        
    })
    searchResult.innerHTML=generatedHTML
}


